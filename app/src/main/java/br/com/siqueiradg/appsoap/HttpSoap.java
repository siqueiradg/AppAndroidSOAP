package br.com.siqueiradg.appsoap;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

public class HttpSoap {

    public static String doPost(String url, String soapXml, String action) throws Exception {

        // SOAP request send
        HttpPost post = new HttpPost(url);
        post.setEntity(new StringEntity(soapXml));
        post.setHeader("Content-type", "text/xml; charset=utf-8; action="+action);

        HttpClient client = new DefaultHttpClient();
        HttpResponse response = client.execute(post);

        // SOAP response(xml) get
        String res_xml = EntityUtils.toString(response.getEntity());
        return res_xml;
    }
}
