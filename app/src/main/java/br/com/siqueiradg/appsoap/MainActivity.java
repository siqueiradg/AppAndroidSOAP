package br.com.siqueiradg.appsoap;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button btn;

    //Dados WS
    private static String URL = "http://agenciaweb.eletrobrasacre.com/wsajuri-homolog/wsordemservico.asmx";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final String xmlSoapEntrada =
                "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                        "  <soap:Body>\n" +
                        "    <GravarFotoOS xmlns=\"http://eletrobras.com/ajuri/webservices/\">\n" +
                        "      <UnidIdentUc>54564</UnidIdentUc>\n" +
                        "      <OserNumeroOs>50</OserNumeroOs>\n" +
                        "      <tipoAnexo>MNB</tipoAnexo>\n" +
                        "      <imagem>base64Binary</imagem>\n" +
                        "    </GravarFotoOS>\n" +
                        "  </soap:Body>\n" +
                        "</soap:Envelope>";

        final String action = "http://eletrobras.com/ajuri/webservices/GravarFotoOS";

        btn = (Button) findViewById(R.id.button);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enviarImagem(xmlSoapEntrada, action);
            }
        });

    }

    private final void enviarImagem(final String x, final String a) {
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    String xmlSoapEntrada = x;
                    String action = a;
                    final String soapXMLRetorno = HttpSoap.doPost(URL, xmlSoapEntrada,action);
                    Log.d("MSG", soapXMLRetorno);
                } catch (Exception e) {
                    Log.e("Falha no envio da imagem: ", e.getMessage());
                }

            }
        }).start();
    }

}


